################################################################################

exit_if_error(){
    if [ "$?" -ne "0" ]; then exit 1; fi
}


#trans="-a split_goal_full -a split_goal_full -a split_goal_full -a split_goal_full"

trans="-a split_goal_full -a split_goal_right"

econf=""
econf="$econf --extra-config PATH_TO_WHY3_CONF_OF_WHY"
econf="$econf --extra-config PATH_TO_ROOT_OF_THIS_REPO/why3-stuff/why3_extra-conf_ergos.conf"

sources_dir="../sources/SPARK-sources/"

here=`pwd`

## Extract VCs for different solvers using the Why3 drivers given in why3-stuff dir
################################################################################
extract_vcs_from_why3_files(){
    mkdir SPARK-VCS-ALL                           ; exit_if_error
    #mkdir SPARK-VCS-ALL/ae-default                ; exit_if_error
    #mkdir SPARK-VCS-ALL/ae-default-inline         ; exit_if_error
    mkdir SPARK-VCS-ALL/ae-fpa                    ; exit_if_error
    mkdir SPARK-VCS-ALL/ae-fpa-inline             ; exit_if_error
    mkdir SPARK-VCS-ALL/ae-no-fpa-and-axioms      ; exit_if_error

    #mkdir SPARK-VCS-ALL/z3-440-default            ; exit_if_error
    mkdir SPARK-VCS-ALL/z3-440-fpa                ; exit_if_error
    mkdir SPARK-VCS-ALL/z3-440-fpa-gnatprove      ; exit_if_error
    #mkdir SPARK-VCS-ALL/z3-440-no-fpa-and-axioms  ; exit_if_error

    #mkdir SPARK-VCS-ALL/cvc4-default              ; exit_if_error
    #mkdir SPARK-VCS-ALL/cvc4-no-fpa-and-axioms    ; exit_if_error

    mkdir SPARK-VCS-ALL/gappa-fpa                 ; exit_if_error
    mkdir SPARK-VCS-ALL/mathsat5-fpa              ; exit_if_error



    cd $sources_dir

    for f in `find spark-whyml/tmp-test-* -name '*'.mlw`
    do

        abs_name=${f%.mlw}
        echo ""
        echo "----------------------------------------------------------------------------------------------------------------------------------------------------"
        echo MLW file = $f
        echo abs name = $abs_name

        #why3 prove -L spark-whyml $f $econf $trans -P AE-IEEE-DEFAULT           -o $here/SPARK-VCS-ALL/ae-default                    ; exit_if_error
        #why3 prove -L spark-whyml $f $econf $trans -P AE-IEEE-DEFAULT-INLINE    -o $here/SPARK-VCS-ALL/ae-default-inline             ; exit_if_error
        why3 prove -L spark-whyml $f $econf $trans -P AE-IEEE-FPA               -o $here/SPARK-VCS-ALL/ae-fpa                        ; exit_if_error
        why3 prove -L spark-whyml $f $econf $trans -P AE-IEEE-FPA-INLINE        -o $here/SPARK-VCS-ALL/ae-fpa-inline                 ; exit_if_error
        why3 prove -L spark-whyml $f $econf $trans -P AE-IEEE-NO-FPA-AXS        -o $here/SPARK-VCS-ALL/ae-no-fpa-and-axioms          ; exit_if_error

        #why3 prove -L spark-whyml $f $econf $trans -P Z3_440-IEEE-DEFAULT       -o $here/SPARK-VCS-ALL/z3-440-default                ; exit_if_error
        why3 prove -L spark-whyml $f $econf $trans -P Z3_440-IEEE-FPA           -o $here/SPARK-VCS-ALL/z3-440-fpa                    ; exit_if_error
        why3 prove -L spark-whyml $f $econf $trans -P Z3_440-IEEE-FPA-GNATPROVE -o $here/SPARK-VCS-ALL/z3-440-fpa-gnatprove          ; exit_if_error
        #why3 prove -L spark-whyml $f $econf $trans -P Z3_440-IEEE-NO-FPA-AXS    -o $here/SPARK-VCS-ALL/z3-440-no-fpa-and-axioms      ; exit_if_error

        #why3 prove -L spark-whyml $f $econf $trans -P CVC4-IEEE-DEFAULT         -o $here/SPARK-VCS-ALL/cvc4-default                  ; exit_if_error
        #why3 prove -L spark-whyml $f $econf $trans -P CVC4-IEEE-NO-FPA-AXS      -o $here/SPARK-VCS-ALL/cvc4-no-fpa-and-axioms        ; exit_if_error

        why3 prove -L spark-whyml $f $econf $trans -P GAPPA-IEEE-FPA            -o $here/SPARK-VCS-ALL/gappa-fpa                     ; exit_if_error

        why3 prove -L spark-whyml $f $econf $trans -P MATHSAT5-IEEE-FPA         -o $here/SPARK-VCS-ALL/mathsat5-fpa                     ; exit_if_error

    done

    cd $here

    #nb_1=`ls SPARK-VCS-ALL/ae-default/*.why | wc -l`
    #nb_2=`ls SPARK-VCS-ALL/ae-default-inline/*.why | wc -l`
    nb_3=`ls SPARK-VCS-ALL/ae-fpa/*.why | wc -l`
    nb_4=`ls SPARK-VCS-ALL/ae-fpa-inline/*.why | wc -l`
    nb_5=`ls SPARK-VCS-ALL/ae-no-fpa-and-axioms/*.why | wc -l`
    #nb_6=`ls SPARK-VCS-ALL/z3-440-default/*.smt2 | wc -l`
    nb_7=`ls SPARK-VCS-ALL/z3-440-fpa/*.smt2 | wc -l`
    nb_8=`ls SPARK-VCS-ALL/z3-440-fpa-gnatprove/*.smt2 | wc -l`
    #nb_9=`ls SPARK-VCS-ALL/z3-440-no-fpa-and-axioms/*.smt2 | wc -l`
    #nb_10=`ls SPARK-VCS-ALL/cvc4-default/*.smt2 | wc -l`
    #nb_11=`ls SPARK-VCS-ALL/cvc4-no-fpa-and-axioms/*.smt2 | wc -l`
    nb_12=`ls SPARK-VCS-ALL/gappa-fpa/*.gappa | wc -l`
    nb_13=`ls SPARK-VCS-ALL/mathsat5-fpa/*.smt2 | wc -l`

    echo ""
    #echo "SPARK-VCS-ALL/ae-default               : $nb_1"
    #echo "SPARK-VCS-ALL/ae-default-inline        : $nb_2"
    echo "SPARK-VCS-ALL/ae-fpa                   : $nb_3"
    echo "SPARK-VCS-ALL/ae-fpa-inline            : $nb_4"
    echo "SPARK-VCS-ALL/ae-no-fpa-and-axioms     : $nb_5"
    #echo "SPARK-VCS-ALL/z3-440-default           : $nb_6"
    echo "SPARK-VCS-ALL/z3-440-fpa               : $nb_7"
    echo "SPARK-VCS-ALL/z3-440-fpa-gnatprove     : $nb_8"
    #echo "SPARK-VCS-ALL/z3-440-no-fpa-and-axioms : $nb_9"
    #echo "SPARK-VCS-ALL/cvc4-default             : $nb_10"
    #echo "SPARK-VCS-ALL/cvc4-no-fpa-and-axioms   : $nb_11"
    echo "SPARK-VCS-ALL/gappa-fpa                : $nb_12"
    echo "SPARK-VCS-ALL/mathsat5-fpa             : $nb_13"
}

## Sanitize MathSAT5 BENCHS
################################################################################
no_bool_args_for_mathsat5(){
    # remove useless, but insupported mathsat5 functions
    cd SPARK-VCS-ALL/mathsat5-fpa
    for f in `ls *.smt2`
    do
        #echo $f
        rm -rf /tmp/temp.mathsat5-fpa
        grep -v "(declare-fun match_bool (ty Bool uni uni) uni)" $f | \
        grep -v "(declare-fun index_bool (Bool) Int)" | \
        grep -v "(declare-fun mk_bool__ref (Bool) uni)" | \
        grep -v "(declare-fun ite1 (ty Bool uni uni) uni)" | \
        grep -v "(declare-fun attr__ATTRIBUTE_IMAGE (Bool) uni)" > /tmp/temp.mathsat5-fpa
        mv /tmp/temp.mathsat5-fpa $f
    done
    cd $here
}

# Main entry
################################################################################
extract_vcs_from_why3_files;
no_bool_args_for_mathsat5;
exit 0
