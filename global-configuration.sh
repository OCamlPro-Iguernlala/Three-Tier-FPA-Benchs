###
# Default configuration: assume that
#   - Three-Tier-FPA-Benchs is in $HOME directory
#   - .opam dir is in $HOME directory
#   - why, why3 and frama-c are installed in .opam/4.03.0 switch
#
# Otherwise, you should configure the right paths in "subst" before
# executing this script
###

substs=""
substs="$substs PATH_TO_WHY3_CONF_OF_WHY:$HOME/.opam/4.03.0/lib/why/why3/why3.conf"
substs="$substs PATH_TO_ROOT_OF_THIS_REPO:$HOME/Three-Tier-FPA-Benchs"

target=""
target="$target generation-scripts/translate-SMT2-benchs.sh"
target="$target generation-scripts/VCs-of-C-programs.sh"
target="$target generation-scripts/VCs-of-SPARK-programs.sh"
target="$target why3-stuff/why3_extra-conf_ergos.conf"

for subst in $substs
do
    echo ""
    echo "subst = $subst"
    key=`echo $subst | cut -f 1 -d":"`
    value=`echo $subst | cut -f 2 -d":"`
    echo "   key = $key"
    echo "   value = $value"

    for file in $target
    do
        echo "      subst in file = $file"
        sed -i -e "s@$key@$value@g" $file
    done

done
